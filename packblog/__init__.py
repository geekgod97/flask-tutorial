from flask import Flask
from flask_sqlalchemy import SQLAlchemy 
from flask_bcrypt import Bcrypt


app = Flask(__name__)
app.config['SECRET_KEY'] = 'c2997b01d2592aa790f4e24dc5af435d'


app.config['SQLALCHEMY_DATABASE_URI']="sqlite:///database.db"
database = SQLAlchemy(app)
bcrypt = Bcrypt(app)

import packblog.routes