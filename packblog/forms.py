from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, PasswordField, TextAreaField
from wtforms.validators import Email, Length, DataRequired, EqualTo

class RegisterForm(FlaskForm):
	username = StringField('Username', validators=[Length(min=2, max=10), DataRequired()])
	email = StringField('Email', validators=[Email(), DataRequired()])
	password = PasswordField('Password', validators=[Length(min=8, max=20), DataRequired()])
	confirm_password = PasswordField('Confirm Password', validators=[Length(min=8, max=20), EqualTo('password'), DataRequired()])
	submit = SubmitField('Register')

class SignInForm(FlaskForm):
	email = StringField('Email: ', validators=[DataRequired(), Email()])
	password = PasswordField('Enter Password: ', validators=[DataRequired(), Length(min=8, max=20)])
	submit = SubmitField('Sign in')

class CreatePostForm(FlaskForm):
	title = StringField("Title: ", validators=[DataRequired()])
	content = TextAreaField("Content: ", validators=[DataRequired()])
	submit = SubmitField("Post")
