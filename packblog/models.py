
from packblog import database 

class User(database.Model):
	_id = database.Column(database.Integer, primary_key=True)
	username =  database.Column(database.String(30), unique=True, nullable=False)
	email =  database.Column(database.String(50), unique=True, nullable=False)
	password =  database.Column(database.String(100), nullable=False)
	post = database.relationship("Post", backref="author")

	def __repr__(self):
		return ("[{"+self.username+"}, {"+self.email+"}]")

class Post(database.Model):

	_id = database.Column(database.Integer, primary_key=True)
	title =  database.Column(database.String(50), unique=False, nullable=False)
	content =  database.Column(database.String(500), unique=False, nullable=False)
	user_id =  database.Column(database.Integer, database.ForeignKey('user._id'), nullable=False)







