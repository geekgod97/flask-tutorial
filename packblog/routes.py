from packblog.forms import RegisterForm, SignInForm, CreatePostForm
from packblog import app, database, bcrypt
from flask import render_template, request, redirect, flash, url_for
from packblog.models import User, Post



@app.route('/')
def home():
	posts = Post.query.all()
	return render_template('home.html', posts=posts)

@app.route('/about')
def about():
	print(request.args.get('p'))
	return render_template('about.html', title='about us')

@app.route('/register', methods=['GET', 'POST'])
def register():
	reg_form = RegisterForm()
	if reg_form.validate_on_submit():
		print('the form is validated')
		temp_pass = bcrypt.generate_password_hash(reg_form.password.data)
		user = User(username=reg_form.username.data, email=reg_form.email.data, password=temp_pass)
		database.session.add(user)
		database.session.commit()
		flash('You have succesfully registered in')
		return redirect(url_for('home'))
	else: 
		print('form has errors')
		print(reg_form.errors)
	return render_template('register.html', reg_form = reg_form)

@app.route('/signin', methods=['POST', 'GET'])
def signin():
	signinForm  = SignInForm()
	if signinForm.validate_on_submit():
		print('form is validated')
		flash('you have been logged in succesfully')
		user = User.query.filter_by(email=signinForm.email).first()
		if user:
			pass
		else:
			print("the user does not exist")
		return redirect(url_for('home'))
	else:
		print('validation error')
		print(signinForm.email.errors)
		print(signinForm.password.errors)
	return render_template('signin.html', form = signinForm)

@app.route('/create_post', methods=['GET', 'POST'])
def create():
	createPostForm = CreatePostForm()
	if createPostForm.validate_on_submit():
		flash('You have posted succesfully!')
		temp_post = Post(title = createPostForm.title.data, content=createPostForm.content.data, user_id=1)
		database.session.add(temp_post)
		database.session.commit() 
		return redirect(url_for('home'))
	return render_template('createPost.html', form=createPostForm)
